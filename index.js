
const FIRST_NAME = "Diana";
const LAST_NAME = "Caluian";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value==Infinity || value==(-Infinity))
        return NaN;
    if(value>Number.MAX_SAFE_INTEGER || value<Number.MIN_SAFE_INTEGER)
        return NaN;
    if(isNaN(value))
        return NaN;
    return parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

